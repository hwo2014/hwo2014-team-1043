﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization; // Added refrence specifically for CSV output, most likely need to be de-refrenced for the bots.cs although not an issue if not used for actual game play.
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Collections;

#if DEBUG
using System.Diagnostics; // Only needed for exporting to CSV
using Sea_Sharp.CSV; // Only needed for exporting to CSV
#endif

namespace Sea_Sharp
{

    public enum RaceLane
    {
        Left = 0,
        Right = 1
    }

    public class Bot
    {
        private BotId _ourBot;

        private bool _isQualifying = true;
        private bool _crashed = false;

        private Race _race;
        private TurboData _turbo;

        private int _targetDirection = 0;
        private bool _needToTurbo = false;
        private float _throttle = 1.0f;
        private int _longestStraightPieceId;
        private bool _isTurboAvailable = false;
        private bool _isTurboActive = false;
        private int _finalStraightStartId;

        private bool _needToSwitch = false;
        private bool _weHaveSentSwithMessage = false;
        private bool _areWeStillInTheSwitch = false;
        private int _lastSwitchId = 999;
        private int _currentSwitchId = 999;
        private int _blockingCarId;
        private int _shuntingCarId;

        private int _currentRanking = 0;
        private int _currentLap = 0;

#if DEBUG
        // quick counters for number of each message. e.g. am I spaming switch!!!
        private int switchMsg = 0;
        private int turboMsg = 0;
        private int throttleMsg = 0;
#endif

        private int _lastPieceIndex = 0;
        private double _lastSpeed = 0.0;
        private double _lastAcceleration = 0.0;
        private double _lastDeltaAcceleration = 0.0;
        private double _lastTrackLength = 0.0;
        private double _lastInPieceDistance = 0.0;
        private double _ideal = 4.0;
        private double _largestA = 0.0;
        private double _largestR = 0.0;
        private double _smallestA = 180.0;
        private double _smallestR = 100.0;
        private double _averageA = 0.0;
        private double _averageR = 0.0;
        private bool _isCrashed = false;
        private double _maxAngle = 0.0;
        private double _lastAngle = 0.0;
        private List<RaceData> _raceData = new List<RaceData>();
        private RaceData _currentRaceData;
        private int _localTickCount = 0;
        private bool _updateFlag = false;
        private int _lastIndex = 0;

        private int _crashedCount = 0;

        OrderedDictionary _preferedLanes = new OrderedDictionary();
        Dictionary<string, Car> _opponents = new Dictionary<string, Car>();

        public static void Main(string[] args)
        {
            int numberOfArgs = args.Length;

            string host;
            int port;
            BotId botId;
            int carCount;
            string trackName;
            string password;
            string messageType = "";
            float throttle;

            Join join;

            switch (numberOfArgs)
            {
                case 4:
                    host = args[0];
                    port = int.Parse(args[1]);
                    botId = new BotId(args[2], args[3]);
                    join = new Join(botId);
                    Console.WriteLine("Connecting to " + host + ":" + port + " as " + botId.name + "/" + botId.key);
                    Console.WriteLine(DateTime.Now.ToString());
                    break;
                case 7:
                    host = args[0];
                    port = int.Parse(args[1]);
                    botId = new BotId(args[2], args[3]);
                    trackName = args[4];
                    carCount = int.Parse(args[5]);
                    messageType = (args[6]);
                    join = new Join(botId, carCount, trackName);
                    Console.WriteLine("Connecting to " + host + ":" + port + " as " + botId.name + "/" + botId.key + " with " + carCount + " on " + trackName);
                    break;
                case 8:
                    host = args[0];
                    port = int.Parse(args[1]);
                    botId = new BotId(args[2], args[3]);
                    trackName = args[4];
                    password = args[5];
                    carCount = int.Parse(args[6]);
                    messageType = args[7];
                    join = new Join(botId, password, carCount, trackName);
                    Console.WriteLine("Connecting to " + host + ":" + port + " pass: " + password + "\nas " + botId.name + "/" + botId.key + " with " + carCount + "'s on " + trackName);
                    break;
                case 9:
                    host = args[0];
                    port = int.Parse(args[1]);
                    botId = new BotId(args[2], args[3]);
                    trackName = args[4];
                    password = args[5];
                    carCount = int.Parse(args[6]);
                    messageType = args[7];
                    throttle = float.Parse(args[8]);
                    join = new Join(botId, password, carCount, trackName, throttle);
                    Console.WriteLine("Connecting to " + host + ":" + port + " pass: " + password + "\nas " + botId.name + "/" + botId.key + " with " + carCount + " cars's on " + trackName + " with a speed of " + throttle);
                    break;
                default:
                    host = args[0];
                    port = int.Parse(args[1]);
                    botId = new BotId(args[2], args[3]);
                    join = new Join(botId);
                    Console.WriteLine("Arguments Errors! Probably.. Well I'm not handling that number!");
                    break;
            }

            using (TcpClient client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                new Bot(reader, writer, join, messageType);
            }

            Console.ReadLine();
        }

        private StreamWriter writer;

        Bot(StreamReader reader, StreamWriter writer, Join join, string messageType)
        {
            this.writer = writer;
            string line;

            _ourBot = new BotId(join.name, join.key);

            switch (messageType)
            {
                case "createRace":
                    send(new Create { botId = _ourBot, carCount = join.carCount, trackName = join.trackName, password = join.password });
                    _throttle = join.throttle;
                    break;
                case "joinRace":
                    send(new JoinRace { botId = _ourBot, carCount = join.carCount, trackName = join.trackName, password = join.password });
                    _throttle = join.throttle;
                    break;
                default:
                    send(join);
                    break;
            }

            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                switch (msg.msgType)
                {
                    case "carPositions":
                        RaceDataCollection(msg.data);
                        RacePlan();
                        break;
                    case "join":
                        Console.WriteLine("Joined");
                        break;
                    case "gameInit":
                        Console.WriteLine("Race init");
                        if (_isQualifying)
                        {
                            Console.WriteLine("Qualifying");
                            SetupRace(msg.data);
                            TrackCalculations();
                            AddOpponents();
                            //   FindLongestStraight(); //not actually used
                            FindPreferedLanes();
                            FindFinalStraightStart();
                            CalculateCorners();
                        }
                        else
                        {
                            Console.WriteLine("Race Time!");
                        }
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        _isQualifying = false;
#if DEBUG
                        ExportDate(join);
                        printTrack();
#endif
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Ping());
                        break;
                    case "turboAvailable":
                        Console.WriteLine("Turbo Available");
                        SetupTurbo(msg.data);
                        break;
                    case "createRace":
                        Console.WriteLine("Created Race");
                        break;
                    case "crash":
                        Crashed(msg.data);
                        break;
                    case "spawn":
                        Spawned(msg.data);
                        break;
                    case "lapFinished":
                        LapFinished(msg.data);
                        break;
                    case "yourCar":
                        Console.WriteLine("yourCar");
                        break;
                    case "finish":
                        Console.WriteLine("finish");
                        break;
                    case "tournamentEnd":
                        Console.WriteLine("tournamentEnd");
                        Console.WriteLine("T:" + turboMsg + " T:" + throttleMsg + " S:" + switchMsg + " C:" + _crashedCount);
                        Console.WriteLine("Track: " + _race.Track.ID.ToString());
                        break;
                    case "error":
                        Console.WriteLine("Error: " + msg.msgType + " Msg:" + msg.data); // We should account for messages coming here!.
                        break;
                    default:
                        Console.WriteLine("Default: " + msg.msgType); // We should account for messages coming here!.
                        break;
                }
            }
        }

        private void send(SendMsg msg)
        {
            writer.WriteLine(msg.ToJson());
        }

        #region Pre-Race Processing

        /// <summary>
        /// Find the start of the longest straight on the track.
        /// It does one loop or all the track pieces, and then starts another loop if it ended on a straight, to end that straight.
        /// Returns the pieceIndex of the starting length, of the longest straight.
        /// </summary>
        /// <param name=" _race.Track.Pieces"></param>
        /// <returns>straightPieceId</returns>
        private void FindLongestStraight()
        {
            bool lastPieceWasAStraight = false;
            int straightCount = 0;
            int highestStraightCount = 0;
            _longestStraightPieceId = 9999;
            int tempStraightPieceId = 9999;

            // Loop of track pieces to work out longest straight
            for (int i = 0; i < _race.Track.NoOfPieces; i++)
            {
                if (_race.Track.Pieces[i].PieceType == PieceType.Straight)
                {
                    straightCount = straightCount + 1;

                    if (!lastPieceWasAStraight)
                    {
                        tempStraightPieceId = i;
                    }

                    lastPieceWasAStraight = true;
                }
                else
                {
                    if (straightCount > highestStraightCount)
                    {
                        highestStraightCount = straightCount;
                        _longestStraightPieceId = tempStraightPieceId;
                    }

                    lastPieceWasAStraight = false;
                }
            }

            //We quickly check to see if the last piece was a straight and next piece is.
            //We do this because the above implementation only goes from start to end, where the start + end could be a really long straight
            // Typically is so really need to refactor this.
            if (lastPieceWasAStraight)
            {
                while (lastPieceWasAStraight)
                {
                    for (int i = 0; i < _race.Track.Pieces.Count; i++)
                    {
                        if (_race.Track.Pieces[i].PieceType == PieceType.Straight)
                        {
                            straightCount = straightCount + 1;

                            if (!lastPieceWasAStraight)
                            {
                                tempStraightPieceId = i;
                            }

                            lastPieceWasAStraight = true;
                        }
                        else
                        {
                            if (straightCount > highestStraightCount)
                            {
                                highestStraightCount = straightCount;
                                _longestStraightPieceId = tempStraightPieceId;
                            }

                            lastPieceWasAStraight = false;
                            break;
                        }
                    }
                }
            }
            Console.WriteLine("Found LongestStraight: " + _longestStraightPieceId);
        }

        /// <summary>
        /// Create _race object from the gameInit
        /// </summary>
        /// <param name="data"></param>
        private void SetupRace(Object data)
        {
            string json = data.ToString();
            Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            string raceJson = dict["race"].ToString();
            Dictionary<string, object> raceDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(raceJson);

            _race = new Race(raceDict);

            Console.WriteLine("Added Track Pieces");
        }

        /// <summary>
        /// Based off the _race object do some calculations e.g. Corner Length
        /// </summary>
        private void TrackCalculations()
        {
            double _radius;
            double _angle;

            for (int i = 0; i < _race.Track.Pieces.Count; i++)
            {
                if (_race.Track.Pieces[i].PieceType != PieceType.Straight)
                {
                    _averageA = _averageA + _race.Track.Pieces[i].Angle;
                    _averageR = _averageR + _race.Track.Pieces[i].Radius;

                    if (_largestA < _race.Track.Pieces[i].Angle)
                    {
                        _largestA = _race.Track.Pieces[i].Angle;
                    }
                    else if (_smallestA > _race.Track.Pieces[i].Angle)
                    {
                        _smallestA = _race.Track.Pieces[i].Angle;
                    }

                    if (_largestR < _race.Track.Pieces[i].Radius)
                    {
                        _largestR = _race.Track.Pieces[i].Radius;
                    }
                    else if (_smallestR > _race.Track.Pieces[i].Radius)
                    {
                        _smallestR = _race.Track.Pieces[i].Radius;
                    }
                }
            }

            _averageA = _averageA / _race.Track.Pieces.Count;
            _averageR = _averageR / _race.Track.Pieces.Count;

            // Calculate Corner Length, per Lane
            for (int i = 0; i < _race.Track.Pieces.Count; i++)
            {
                if (_race.Track.Pieces[i].PieceType == PieceType.Straight)
                {
                    for (int j = 0; j < _race.Track.NoOfLanes; j++)
                    {
                        _race.Track.Pieces[i].Lanes.Add(new Lane(_race.Track.Pieces[i].Length, _ideal, 0.0));
                    }
                }
                else if (_race.Track.Pieces[i].PieceType != PieceType.Straight)
                {
                    if (_race.Track.Pieces[i].PieceType == PieceType.LeftCorner)
                    {
                        for (int j = 0; j < _race.Track.NoOfLanes; j++)
                        {
                            _radius = _race.Track.Pieces[i].Radius + _race.Track.Lanes[j].DistanceFromCenter;
                            _angle = -_race.Track.Pieces[i].Angle;

                            _race.Track.Pieces[i].Lanes.Add(new Lane((_radius) * ((_angle / 180) * (Math.PI)),
                                BigCorners(_radius, _angle), 0.0));
                        }
                    }
                    else
                    {
                        for (int j = 0; j < _race.Track.NoOfLanes; j++)
                        {
                            _radius = _race.Track.Pieces[i].Radius + _race.Track.Lanes[j].DistanceFromCenter;
                            _angle = _race.Track.Pieces[i].Angle;

                            _race.Track.Pieces[i].Lanes.Add(new Lane((_radius) * ((_angle / 180) * (Math.PI)),
                                BigCorners(_radius, _angle), 0.0));
                        }
                    }
                }
            }
            Console.WriteLine("Calculated Corner Length, per Lane");
        }

        /// <summary>
        /// Assumed we are always going clock-wise. E.g.
        /// Left is negative (thus lane one prefered), and Right is positive (thus lane two prefered).
        /// 
        /// For each cross over section I compare how many time each lane gets the inside lane of a corner
        /// E.g. Left = 2, right = 2..
        /// Left gets two, and right gets two inside lanes, this makes them equal at two a pop BUT the left angles are bigger
        /// E.g. Left = 45, 45(90) Right = 45, 25(70)
        /// So counting the total angle size we can determine that Left saves us more than the smaller angle
        /// </summary>
        private void FindPreferedLanes()
        {
            bool WeAreCounting = false;

            // Key = SwitchId, Dictionary<PreferedLineOrder, Lane>

            float left = 0;
            float right = 0;

            int tempSwitchId = 0;

            bool firstSwitch = true;
            int firstSwitchId = 0;

            for (int i = 0; i < _race.Track.NoOfPieces; i++)
            {
                if (_race.Track.Pieces[i].IsSwitch)
                {
                    if (firstSwitch)
                    {
                        firstSwitch = false;

                        firstSwitchId = i;
                    }

                    if (WeAreCounting)
                    {
                        WeAreCounting = false;

                        Dictionary<int, string> order = new Dictionary<int, string>();

                        if (left > right)
                        {
                            order.Add(1, "0");
                            order.Add(2, "1,2,3");
                        }
                        else if (right > left)
                        {
                            order.Add(1, "1,2,3");
                            order.Add(2, "0");
                        }
                        else // We look to the previous switch section for the best lane to carry on in
                        {
                            var tempDic = (Dictionary<int, string>)_preferedLanes[_preferedLanes.Count - 1];

                            order.Add(1, tempDic[1]);
                            order.Add(2, tempDic[2]);
                        }

                        _preferedLanes.Add(tempSwitchId, order);
                    }

                    if (!WeAreCounting)
                    {
                        WeAreCounting = true;
                        tempSwitchId = i;

                        left = 0;
                        right = 0;
                    }
                }

                if (WeAreCounting)
                {
                    if (_race.Track.Pieces[i].PieceType != PieceType.Straight && !_race.Track.Pieces[i].IsSwitch)
                    {
                        if (_race.Track.Pieces[i].Angle > 0) // Right Corner
                        {
                            right = right + _race.Track.Pieces[i].Angle;
                        }
                        else                                // Left  Corner
                        {
                            left = left + (_race.Track.Pieces[i].Angle * -1); // Cheeky bit of code to make the negative number to a postivie, so I can compare..
                        }
                    }
                }
            }

            // TODO Need to do another bit of a loop to finish off the track..
            // TODO Look at the next lane to see what the best is to switch as the final one will always have the next on available.
            #region Horrible bit of code, need to make into methods!
            if (_preferedLanes.Count < _race.Track.NoOfSwitches)
            {
                for (int i = 0; i < _race.Track.NoOfPieces; i++)
                {
                    if (_race.Track.Pieces[i].IsSwitch)
                    {
                        if (WeAreCounting)
                        {
                            WeAreCounting = false;

                            Dictionary<int, string> order = new Dictionary<int, string>();

                            if (left > right)
                            {
                                order.Add(1, "0");
                                order.Add(2, "1,2,3");
                            }
                            else if (right > left)
                            {
                                order.Add(1, "1,2,3");
                                order.Add(2, "0");
                            }
                            else // We look to the next switch section for the best lane to currently be in if we are equal..
                            {
                                var tempDic = (Dictionary<int, string>)_preferedLanes[firstSwitchId];

                                order.Add(1, tempDic[1]);
                                order.Add(2, tempDic[2]);
                            }

                            _preferedLanes.Add(tempSwitchId, order);
                        }
                    }

                    if (WeAreCounting)
                    {
                        if (_race.Track.Pieces[i].PieceType != PieceType.Straight && !_race.Track.Pieces[i].IsSwitch)
                        {
                            if (_race.Track.Pieces[i].Angle > 0) // Right Corner
                            {
                                right = right + _race.Track.Pieces[i].Angle;
                            }
                            else                                // Left  Corner
                            {
                                left = left + (_race.Track.Pieces[i].Angle * -1); // Cheeky bit of code to make the negative number to a postivie, so I can compare..
                            }
                        }
                    }

                    if (_preferedLanes.Count == _race.Track.NoOfSwitches)
                    {
                        break;
                    }
                }
            }
            #endregion

#if DEBUG

            foreach (DictionaryEntry od in _preferedLanes)
            {
                System.Console.WriteLine("Swtich:" + od.Key + " Lane:" + od.Value);
            }

            //int j = 0;

            //foreach (var pair in _preferedLanes)
            //{
            //    Dictionary<int, Dictionary<int, int>> xPair = (Dictionary<int, Dictionary<int, int>>)pair;

            //    foreach (KeyValuePair<int, Dictionary<int, int>> yPair in xPair)
            //    {

            //        foreach (KeyValuePair<int, int> anotherPair in yPair.Value)
            //        {
            //            Console.WriteLine("Switch:" + j + " Order:" + anotherPair.Key + " Lane:" + anotherPair.Value);
            //        }

            //        j = j + 1;
            //    }
            //}
#endif
            Console.WriteLine("Calculated Prefered Lanes");

        }

        /// <summary>
        /// We loop back from the last piece of track, as this connects to the first piece.
        /// </summary>
        private void FindFinalStraightStart()
        {
            int tempTrackId = 0;
            int trackLength = _race.Track.Pieces.Count - 1;

            if (_race.Track.Pieces[trackLength].PieceType == PieceType.Straight) // Not point going any further if the track starts off a bend!
            {
                for (int i = trackLength; i > 0; i--)
                {
                    if (_race.Track.Pieces[i].PieceType != PieceType.Straight)
                    {
                        _finalStraightStartId = tempTrackId;
                        break;
                    }
                    else
                    {
                        tempTrackId = i;
                    }
                }
            }
            Console.WriteLine("Final Straight Start:" + _finalStraightStartId);
        }

        private void AddOpponents()
        {
            for (int i = 0; i < _race.Cars.cars.Count; i++)
            {
                _opponents.Add(_race.Cars.cars[i].name, _race.Cars.cars[i]);
            }
        }

        private void CalculateCorners()
        {
            double radiousCounter = 0.0;
            float angleCounter = 0.0f;
            List<int> pieceIds = new List<int>();

            for (int i = 0; i < _race.Track.NoOfPieces; i++)
            {
                if (_race.Track.Pieces[i].PieceType != PieceType.Straight)
                {
                    if (i != 0)
                    {
                        if (_race.Track.Pieces[i].PieceType == _race.Track.Pieces[i - 1].PieceType || _race.Track.Pieces[i - 1].PieceType == PieceType.Straight)
                        {
                            radiousCounter = radiousCounter + _race.Track.Pieces[i].Radius;
                            angleCounter = angleCounter + _race.Track.Pieces[i].Angle;
                            pieceIds.Add(i);
                        }
                        else
                        {
                            if (angleCounter < 0)
                            {
                                angleCounter = angleCounter * -1;
                            }

                            string key = "" + angleCounter + ":" + radiousCounter;

                            if (!_race.Track.CornersDic.ContainsKey(key))
                            {
                                List<Corner> tempList = new List<Corner>();
                                tempList.Add(new Corner(radiousCounter, angleCounter, new List<int>(pieceIds)));
                                _race.Track.CornersDic.Add(key, tempList);

                                foreach (int x in pieceIds)
                                {
                                    _race.Track.Pieces[x].cornerKey = key;
                                }
                            }
                            else
                            {
                                _race.Track.CornersDic[key].Add(new Corner(radiousCounter, angleCounter, new List<int>(pieceIds)));

                                foreach (int x in pieceIds)
                                {
                                    _race.Track.Pieces[x].cornerKey = key;
                                }
                            }

                            angleCounter = 0.0f;
                            radiousCounter = 0.0;
                            key = "";
                            pieceIds.Clear();
                        }
                    }
                }
                else if (pieceIds.Count > 0)
                {
                    if (angleCounter < 0)
                    {
                        angleCounter = angleCounter * -1;
                    }

                    string key = "" + angleCounter + ":" + radiousCounter;

                    if (!_race.Track.CornersDic.ContainsKey(key))
                    {
                        List<Corner> tempList = new List<Corner>();
                        tempList.Add(new Corner(radiousCounter, angleCounter, new List<int>(pieceIds)));
                        _race.Track.CornersDic.Add(key, tempList);

                        foreach (int x in pieceIds)
                        {
                            _race.Track.Pieces[x].cornerKey = key;
                        }
                    }
                    else
                    {
                        _race.Track.CornersDic[key].Add(new Corner(radiousCounter, angleCounter, new List<int>(pieceIds)));

                        foreach (int x in pieceIds)
                        {
                            _race.Track.Pieces[x].cornerKey = key;
                        }
                    }

                    angleCounter = 0.0f;
                    radiousCounter = 0.0;
                    key = "";
                    pieceIds.Clear();
                }
            }
        }
        #endregion

        #region Race Time Processing

        /// <summary>
        /// This method is so, so dirty.
        /// </summary>
        /// <param name="Data"></param>
        private void RaceDataCollection(Object Data)
        {
            _localTickCount = _localTickCount + 1;

            string posJSON = Data.ToString();

            List<Dictionary<string, object>> rawData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(posJSON);

            //For each Race Car
            for (int i = 0; i < rawData.Count; i++)
            {
                Dictionary<string, object> allData = rawData[i];
                Dictionary<string, object> idData = JsonConvert.DeserializeObject<Dictionary<string, object>>(allData["id"].ToString());

                if (idData["name"].Equals(_ourBot.name))
                {
                    Dictionary<string, object> posData = JsonConvert.DeserializeObject<Dictionary<string, object>>(allData["piecePosition"].ToString());
                    int _pieceIndex = Convert.ToInt32(posData["pieceIndex"]);
                    double _inPieceDistance = Convert.ToDouble(posData["inPieceDistance"]);
                    double _angle = Convert.ToDouble(allData["angle"]);
                    int _lap = Convert.ToInt16(posData["lap"]);

                    bool _isSwitch = _race.Track.Pieces[_pieceIndex].IsSwitch;

                    Dictionary<string, object> laneData = JsonConvert.DeserializeObject<Dictionary<string, object>>(posData["lane"].ToString());
                    int _currentLane = Convert.ToInt32(laneData["endLaneIndex"]);

                    double _trackLength;

                    if (_race.Track.Pieces[_pieceIndex].PieceType != PieceType.Straight)
                    {
                        _trackLength = _race.Track.Pieces[_pieceIndex].Lanes[_currentLane].Length;
                    }
                    else
                    {
                        _trackLength = _race.Track.Pieces[_pieceIndex].Length;
                    }

                    //Speed stuff
                    double _currentSpeed = 0.0;
                    double _currentAcceleration = 0.0;
                    double _currentDeltaAcceleration = 0.0;

                    if (_lastPieceIndex == _pieceIndex)
                    {
                        _currentSpeed = SpeedCalculation(Math.Abs(_inPieceDistance), Math.Abs(_lastInPieceDistance));
                    }
                    else
                    {
                        _updateFlag = true;
                        _lastIndex = _lastPieceIndex;
                        _currentSpeed = PositionCalculation(Math.Abs(_lastTrackLength), Math.Abs(_inPieceDistance), Math.Abs(_lastInPieceDistance));
                    }

                    _currentAcceleration = SpeedCalculation(Math.Abs(_currentSpeed), Math.Abs(_lastSpeed));
                    _currentDeltaAcceleration = SpeedCalculation(Math.Abs(_currentAcceleration), Math.Abs(_lastAcceleration));
                    //End Speed Stuff

                    _currentRaceData = new RaceData
                    {
                        localTickCount = _localTickCount,
                        angle = _angle,
                        lap = _lap,
                        lane = _currentLane,
                        inPieceDistance = _inPieceDistance,
                        pieceIndex = _pieceIndex,
                        trackLength = _trackLength,
                        trackType = _race.Track.Pieces[_pieceIndex].PieceType.ToString(),
                        trackRadius = _race.Track.Pieces[_pieceIndex].Radius,
                        trackAngle = _race.Track.Pieces[_pieceIndex].Angle,
                        thorttle = _throttle,
                        turboActive = _isTurboActive,
                        currentSpeed = _currentSpeed,
                        currentAcceleration = _currentAcceleration,
                        currentDeltaAcceleration = _currentDeltaAcceleration,
                        isSwitch = _isSwitch,
                        crashed = _crashed
                    };

                    //Speed Stuff Updating
                    _lastPieceIndex = _pieceIndex;
                    _lastSpeed = _currentSpeed;
                    _lastAcceleration = _currentAcceleration;
                    _lastDeltaAcceleration = _currentDeltaAcceleration;
                    _lastTrackLength = _trackLength;
                    _lastInPieceDistance = _inPieceDistance;
                    //End Speed Stuff Updating

                    _raceData.Add(_currentRaceData);
                }
                else
                {
                    Dictionary<string, object> posData = JsonConvert.DeserializeObject<Dictionary<string, object>>(allData["piecePosition"].ToString());
                    int _pieceIndex = Convert.ToInt32(posData["pieceIndex"]);
                    double _inPieceDistance = Convert.ToDouble(posData["inPieceDistance"]);

                    Dictionary<string, object> laneData = JsonConvert.DeserializeObject<Dictionary<string, object>>(posData["lane"].ToString());
                    int _currentLane = Convert.ToInt32(laneData["startLaneIndex"]);
                    int _endLane = Convert.ToInt32(laneData["endLaneIndex"]);

                    if (_opponents.ContainsKey(idData["name"].ToString()))
                    {
                        _opponents[idData["name"].ToString()].pieceIndex = _pieceIndex;
                        _opponents[idData["name"].ToString()].inPieceDistance = _inPieceDistance;
                        _opponents[idData["name"].ToString()].startLane = _currentLane;
                        _opponents[idData["name"].ToString()].endLane = _endLane;
                    }
                }
            }
        }

        private void RacePlan()
        {
            IsThereACarBehindUs();

            _areWeStillInTheSwitch = HaveWeMovedPassTheSwitchYet();

            if (_areWeStillInTheSwitch)
            {
                _weHaveSentSwithMessage = false;
            }

            TrackNavigation();

            // Fuzzy Logic Code..
            double _idealSpeed = 5.0;
            double _currentSpeed = _currentRaceData.currentSpeed;
            double _currentAngle = Math.Abs(_currentRaceData.angle);
            int thisPiece = _currentRaceData.pieceIndex;
            double _deltaAngle = _lastAngle - _currentAngle;

            // Are we checking we are on a corner?
            if (_currentAngle > _maxAngle)
            {
                _maxAngle = _currentAngle;
            }

            //End of a Track Piece Updating
            if (_updateFlag)
            {
                if ((_race.Track.Pieces[_lastIndex].PieceType != PieceType.Straight) && (!_race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].metMaxV))
                {
                    if (_maxAngle >= 30 && _isCrashed == false)
                    {
                        if (_race.Track.ID.Equals("france") || _race.Track.ID.Equals("pentag"))
                        {
                            SetMapSpecificSpeed(_lastIndex, 0.0f, false);
                        }
                        else
                        {
                            SetSameCornerSpeedNormal(_lastIndex, 0.0f, true);
                        }
                    }
                    else if (_maxAngle >= 20 && _isCrashed == false)
                    {
                        if (_race.Track.ID.Equals("france") || _race.Track.ID.Equals("pentag"))
                        {
                            SetMapSpecificSpeed(_lastIndex, 0.1f, false);
                        }
                        else
                        {
                            SetSameCornerSpeedNormal(_lastIndex, 0.05f, false);
                        }
                    }
                    else if (_maxAngle >= 10 && _isCrashed == false)
                    {
                        if (_race.Track.ID.Equals("france") || _race.Track.ID.Equals("pentag"))
                        {
                            SetMapSpecificSpeed(_lastIndex, 0.2f, false);
                        }
                        else
                        {
                            SetSameCornerSpeedNormal(_lastIndex, 0.1f, false);
                        }
                    }
                    else if (_isCrashed == false)
                    {
                        if (_race.Track.ID.Equals("france") || _race.Track.ID.Equals("pentag"))
                        {
                            SetMapSpecificSpeed(_lastIndex, 0.3f, false);
                        }
                        else
                        {
                            SetSameCornerSpeedNormal(_lastIndex, 0.2f, false);
                        }
                    }
                }
                _maxAngle = 0.0;
                _updateFlag = false;
            }

            //Cornering Speed control

            if (_race.Track.Pieces[thisPiece].PieceType != PieceType.Straight)
            {
                CornerThrottle(_idealSpeed, _currentSpeed, thisPiece);
            }

            // Do the same for switches?
            // slow down for approaching corner?
            // makes sure we aren't on last piece, and have idealV data after one lap..
            if (thisPiece + 1 <= _race.Track.NoOfPieces - 1 && _currentRaceData.lap > 1)
            {
                if (_race.Track.Pieces[thisPiece + 1].PieceType != PieceType.Straight)
                {
                    CornerThrottle(_idealSpeed, _currentSpeed, thisPiece);
                }
            }

            if (_race.Track.Pieces[thisPiece].PieceType == PieceType.Straight && _race.Track.Pieces[(thisPiece + 1) % _race.Track.Pieces.Count].PieceType == PieceType.Straight)
            {
                _throttle = 1.0f;
            }
            if (_race.Track.Pieces[thisPiece].PieceType == PieceType.Straight && _race.Track.Pieces[(thisPiece + 1) % _race.Track.Pieces.Count].PieceType != PieceType.Straight && (_currentSpeed > _race.Track.Pieces[(thisPiece + 1) % _race.Track.Pieces.Count].Lanes[_currentRaceData.lane].idealV))
            {
                _throttle = 0.01f;
            }
            if (_currentSpeed < 1)
            {
                _throttle = _throttle + 0.2f;
            }
            if (_throttle <= 0.0f)
            {
                _throttle = 0.01f;
            }
            if (_throttle > 1.0f)
            {
                _throttle = 0.99f;
            }
            if (_currentAngle >= 30 && _currentAngle > _lastAngle)
            {
                _throttle = _throttle / 2;
            }
            if (_currentAngle >= 20 && _deltaAngle >= 3.0)
            {
                _throttle = 0.01f;
            }

            _lastAngle = _currentAngle;

            TurboToFinish();

            FinalSend();
        }

        private void SetSameCornerSpeedNormal(int lastIndex, float speedVariable, bool isMax)
        {
            if (!_race.Track.Pieces[lastIndex].cornerKey.Equals("")) // some corners didn't get added.. :s
            {
                var tempListCorners = (List<Corner>)_race.Track.CornersDic[_race.Track.Pieces[lastIndex].cornerKey];

                foreach (Corner c in tempListCorners)
                {
                    foreach (int p in c.pieceIds)
                    {
                        if (isMax)
                        {
                            _race.Track.Pieces[p].Lanes[_currentRaceData.lane].metMaxV = true;

                            // Set for both lanes as some instaces could go very high>? maybe?
                            for (int i = 0; i < _race.Track.NoOfLanes; i++)
                            {
                                if (_race.Track.Pieces[p].Lanes[_currentRaceData.lane].idealV > _race.Track.Pieces[p].Lanes[i].idealV)
                                {
                                    _race.Track.Pieces[p].Lanes[i].idealV = (_race.Track.Pieces[p].Lanes[i].idealV + _race.Track.Pieces[p].Lanes[_currentRaceData.lane].idealV) / 2;
                                }
                                Console.WriteLine("MAX-VEL-SET TP: " + p + " Lane: " + i + "IdealV: " + _race.Track.Pieces[p].Lanes[i].idealV);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < _race.Track.NoOfLanes; i++)
                            {
                                if (_race.Track.Pieces[p].Lanes[i].metMaxV == false)
                                {
                                    _race.Track.Pieces[p].Lanes[i].idealV = _race.Track.Pieces[p].Lanes[i].idealV + speedVariable;
                                    _race.Track.Pieces[p].Lanes[i].deltaIdealV = speedVariable;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void SetMapSpecificSpeed(int lastIndex, float speedVariable, bool isMax)
        {
            if (isMax)
            {
                _race.Track.Pieces[lastIndex].Lanes[_currentRaceData.lane].metMaxV = true;

                // Set for both lanes as some instaces could go very high>? maybe?
                for (int i = 0; i < _race.Track.NoOfLanes; i++)
                {
                    if (_race.Track.Pieces[lastIndex].Lanes[_currentRaceData.lane].idealV > _race.Track.Pieces[_currentRaceData.lane].Lanes[i].idealV)
                    {
                        _race.Track.Pieces[lastIndex].Lanes[i].idealV = (_race.Track.Pieces[_currentRaceData.lane].Lanes[i].idealV + _race.Track.Pieces[_currentRaceData.lane].Lanes[_currentRaceData.lane].idealV) / 2;
                    }
                    Console.WriteLine("MAX-VEL-SET TP: " + _currentRaceData.lane + " Lane: " + i + "IdealV: " + _race.Track.Pieces[_currentRaceData.lane].Lanes[i].idealV);
                }
            }
            else
            {
                _race.Track.Pieces[lastIndex].Lanes[_currentRaceData.lane].idealV = _race.Track.Pieces[lastIndex].Lanes[_currentRaceData.lane].idealV + speedVariable;
                _race.Track.Pieces[lastIndex].Lanes[_currentRaceData.lane].deltaIdealV = speedVariable;
            }
        }

        private void SetupTurbo(Object data)
        {
            string json = data.ToString();
            Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            _turbo = new TurboData(dict);

            _isTurboAvailable = true;
        }

        private int RelativePieceID(int _currentId, int _dist)
        {
            int _eval = _currentId + _dist;

            if (_eval >= 0)
            {
                return (_eval);
            }
            else if (_eval < 0)
            {
                return (_race.Track.Pieces.Count + _eval);
            }
            else
            {
                return (0);
            }
        }

        private bool HaveWeMovedPassTheSwitchYet()
        {
            if (_race.Track.Pieces[_currentRaceData.pieceIndex].IsSwitch)
            {
                _currentSwitchId = _currentRaceData.pieceIndex;
            }

            if (_currentSwitchId != 999)
            {
                if (_race.Track.Pieces[_currentRaceData.pieceIndex].IsSwitch && _lastSwitchId != _currentRaceData.pieceIndex)
                {
                    _lastSwitchId = _currentSwitchId;

                    return true;
                }
            }
            return false;
        }

        private void CornerThrottle(double _idealSpeed, double _currentSpeed, int thisPiece)
        {
            _idealSpeed = _race.Track.Pieces[thisPiece].Lanes[_currentRaceData.lane].idealV;

            if (_currentSpeed < (_idealSpeed + 0.02))
            {
                if (_currentSpeed < (_idealSpeed + 0.2))
                {
                    _throttle = _throttle + 0.02f;
                }
                _throttle = _throttle + 0.02f;
            }
            if (_currentSpeed > (_idealSpeed - 0.02))
            {
                if (_currentSpeed > (_idealSpeed - 0.2))
                {
                    _throttle = _throttle - 0.2f;
                }
                _throttle = _throttle - 0.1f;
            }
        }

        private void TrackNavigation()
        {
            if (AreWeStuckBehindACar())
            {
                _needToSwitch = DoWeNeedToSwitchForPreferedLane();

                // if our current race line and our target line is the same (still blocked), and the opponents cars end lane is stil our lane we need to switch..
                if ((_currentRaceData.lane == _targetDirection) && (_opponents[_race.Cars.cars[_blockingCarId].name].endLane == _targetDirection))
                {
                    int _previousTargetLane = _targetDirection;
                    _needToSwitch = true;
                    _targetDirection = CrudePickLane(_previousTargetLane);

                }// else our lane is still going to the target, but our opnnent is switching lanes we should stay put..
                else if ((_currentRaceData.lane == _targetDirection) && (_opponents[_race.Cars.cars[_blockingCarId].name].endLane != _targetDirection))
                {

                }
                else
                {

                }
            }
            else
            {
                if (!_weHaveSentSwithMessage)
                {
                    _needToSwitch = DoWeNeedToSwitchForPreferedLane();
                }
            }
        }

        private int CrudePickLane(int _previousTargetLane)
        {
            int targetLane = 0;

            switch (_previousTargetLane)
            {
                case 0:
                    targetLane = 1;
                    break;
                case 1:
                    targetLane = 0;
                    break;
                case 2:
                    targetLane = 0;
                    break;
                case 3:
                    targetLane = 0;
                    break;
                default:

                    break;
            }

            return targetLane;
        }

        private double PositionCalculation(double _lastTrackLength, double _currentInPieceDistance, double _lastInPieceDistance)
        {
            //Only needed when transitioning the track pieces
            //#if DEBUG
            //            Console.WriteLine("Change Piece:");
            //            Console.WriteLine(_lastTrackLength + "= _lastTrackLength");
            //            Console.WriteLine(_currentInPieceDistance +"= _currentInPieceDistance");
            //            Console.WriteLine(_lastInPieceDistance + "= _lastInPieceDistance");
            //#endif
            double _displacement = (_lastTrackLength - _lastInPieceDistance) + _currentInPieceDistance;
            //            Console.WriteLine("***** " +_displacement + "= Displacement ****);
            return (_displacement);
        }

        private double SpeedCalculation(double _new, double _old)
        {
            //Nice and easy...this works for both velocity and acceleration
            double _current = _new - _old;
            return (_current);
        }

        private double BigCorners(double radius, double angle)
        {
            //finding the ideal speed for th einitial _idealV setup
            double _best = _ideal;
            if (radius > (_averageR)) { _best = _best + 0.5; }
            if (radius < (_averageR)) { _best = _best - 0.5; }
            if (radius > (_largestR - _averageR)) { _best = _best + 0.5; }
            if (radius < (_smallestR + _averageR)) { _best = _best - 0.5; }
            if (angle > (80) && (radius < (_largestR - _averageR))) { _best = _best - 0.5; }
            return (_best);
        }

        private bool AreWeStuckBehindACar()
        {
            bool areWeStuckBehindACar = false;

            for (int i = 0; i < _race.Cars.cars.Count; i++)
            {
                // Make sure we don't look at ourself..           Are we in the same piece as the other car?
                if ((_race.Cars.cars[i].name != _ourBot.name) && (_currentRaceData.pieceIndex == _opponents[_race.Cars.cars[i].name].pieceIndex))
                {// Are we in the same lane?
                    if (_currentRaceData.lane == _opponents[_race.Cars.cars[i].name].startLane)
                    {
                        // Is the other car infront of us?
                        if (_currentRaceData.inPieceDistance < _opponents[_race.Cars.cars[i].name].inPieceDistance)
                        {   // inPieceDistance - Car Length = back of opponent, then if our inPieceDistance + 15 then we should over take)
                            if ((_opponents[_race.Cars.cars[i].name].inPieceDistance - _opponents[_race.Cars.cars[i].name].length) < _currentRaceData.inPieceDistance + (_opponents[_race.Cars.cars[i].name].length / 2))
                            {
                                areWeStuckBehindACar = true;

                                _blockingCarId = i;

                                //          Console.WriteLine("Are we STUCK behind a car? " + areWeStuckBehindACar + " Who? " + _opponents[_race.Cars.cars[i].name].name);
                            }
                        }
                    }
                }
            }
            return areWeStuckBehindACar;
        }

        private bool IsThereACarBehindUs()
        {
            bool isThereACarBehindUs = false;

            for (int i = 0; i < _race.Cars.cars.Count; i++)
            {
                // Make sure we don't look at ourself..           Are we in the same piece as the other car?
                if ((_race.Cars.cars[i].name != _ourBot.name) && (_currentRaceData.pieceIndex == _opponents[_race.Cars.cars[i].name].pieceIndex))
                {// Are we in the same lane?
                    if (_currentRaceData.lane == _opponents[_race.Cars.cars[i].name].startLane)
                    {
                        // Is the other car behind of us?
                        if (_currentRaceData.inPieceDistance > _opponents[_race.Cars.cars[i].name].inPieceDistance)
                        {// if back of our car (inPieceDistance - Car Length) is less then our oppents inPieceDistance + half the car length THEN on our tail!	
                            if ((_currentRaceData.inPieceDistance - _opponents[_race.Cars.cars[i].name].length) < _opponents[_race.Cars.cars[i].name].inPieceDistance + (_opponents[_race.Cars.cars[i].name].length / 3))
                            {
                                isThereACarBehindUs = true;
                                _shuntingCarId = i;

                                //       Console.WriteLine("Is There A Car BEHIND Us? " + isThereACarBehindUs + " Who? " + _opponents[_race.Cars.cars[i].name].name);
                            }
                        }
                    }
                }
            }
            return isThereACarBehindUs;
        }

        private bool DoWeNeedToSwitchForPreferedLane()
        {
            bool needToSwitchForPreferedLane = false;

            if (_race.Track.Pieces.Count != _currentRaceData.pieceIndex + 1) // Annoying if, so I don't cause an exception below when checking the track infront of us.
            {
                if (_race.Track.Pieces[_currentRaceData.pieceIndex + 1].IsSwitch)
                {
                    object tempObj = _currentRaceData.pieceIndex + 1;

                    var idealLaneDic = (Dictionary<int, string>)_preferedLanes[tempObj];

                    // If lane prefered lane is 0, AND we aren't in said lane, go LEFT!
                    if ((idealLaneDic[1].Contains("0")) && (!idealLaneDic[1].Contains(_currentRaceData.lane.ToString())))
                    {
                        _targetDirection = 0;
                        needToSwitchForPreferedLane = true;
                        //        Console.WriteLine("Need To Switch For Prefered Lane " + needToSwitchForPreferedLane);
                    }// If lane prefered is NOT 0, AND we are in either 1,2,3..
                    else if ((!idealLaneDic[1].Contains("0")) && (_race.Track.NoOfLanes > _currentRaceData.lane + 1))
                    {
                        _targetDirection = 1;
                        needToSwitchForPreferedLane = true;
                        //       Console.WriteLine("Need To Switch For Prefered Lane " + needToSwitchForPreferedLane);
                    }
                    else
                    { // Should be in prefered lane?
                        needToSwitchForPreferedLane = false;
                        //  Console.WriteLine("Need To Switch For Prefered Lane " + needToSwitchForPreferedLane);
                    }
                }
            }

            return needToSwitchForPreferedLane;
        }

        private void Crashed(Object Data)
        {
            string posJSON = Data.ToString();

            Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(posJSON);

            int _ticking = 0;

            if (rawData["name"].Equals(_ourBot.name))
            {
                _crashed = true;
                _isCrashed = true; //flag for the slowing down (should only happen once so we need to be careful)
                _crashedCount = _crashedCount + 1;
                Console.WriteLine("Crashed!");

                if (_raceData.Count > 50)
                {
                    _ticking = 50;
                }
                else
                {
                    _ticking = _raceData.Count;
                }

                for (int i = 0; i < _ticking - 1; i++)
                {
                    Console.WriteLine("TP:" + _raceData[(_raceData.Count - 1) - i].pieceIndex + "TA:" + _raceData[(_raceData.Count - 1) - i].trackAngle + "S:" + _raceData[(_raceData.Count - 1) - i].currentSpeed + "A:" + _raceData[(_raceData.Count - 1) - i].currentAcceleration + "T:" + _raceData[(_raceData.Count - 1) - i].thorttle + "CA:" + _raceData[(_raceData.Count - 1) - i].angle + " " + _race.Track.Pieces[_raceData[(_raceData.Count - 1) - i].pieceIndex].Lanes[0].idealV);
                }

                CrashedTrackUpdate();
            }
        }

        private void TurboToFinish()
        {
            if (!_isQualifying || _race.RaceSession.quickRace)
            {
                if (_currentRaceData.pieceIndex == _finalStraightStartId && _currentRaceData.lap == (_race.RaceSession.laps - 1) && _isTurboAvailable)
                {
                    _needToTurbo = true;
                    _throttle = 1.0f;
                    _isTurboActive = true;
                }
            }
        }

        private void FinalSend()
        {
            if (_needToSwitch)
            {
                send(new SwitchLane(_targetDirection));
                _needToSwitch = false;
                _weHaveSentSwithMessage = true;
#if DEBUG
                switchMsg = switchMsg + 1;
#endif
            }
            else if (_needToTurbo)
            {
                send(new Turbo());
                _needToTurbo = false;
#if DEBUG
                turboMsg = turboMsg + 1;
#endif
            }
            else
            {
                send(new Throttle(_throttle));
#if DEBUG
                throttleMsg = throttleMsg + 1;
#endif
            }
        }

        private void LapFinished(Object Data)
        {
            string posJSON = Data.ToString();

            Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(posJSON);
            Dictionary<string, object> carData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData["car"].ToString());

            if (carData["name"].Equals(_ourBot.name))
            {
                Dictionary<string, object> rankingData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData["ranking"].ToString());
                Dictionary<string, object> lapTime = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData["lapTime"].ToString());

                _currentRanking = Convert.ToInt32(rankingData["overall"]);
                _currentLap = Convert.ToInt32(lapTime["lap"]);

                Console.WriteLine("Lap:" + _currentLap + " Ranking:" + _currentRanking);
            }
        }

        private void Spawned(Object Data)
        {
            string posJSON = Data.ToString();

            Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(posJSON);

            if (rawData["name"].Equals(_ourBot.name))
            {
                Console.WriteLine("Spawned: " + _ourBot.name + " Crash Count:" + _crashedCount);
            }
        }

        private void CrashedTrackUpdate()
        {
            _isCrashed = false;

            if (!IsThereACarBehindUs())
            {
                //if virgin track, then we half the _ideal, else take off the last input and call it a day
                if (_race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].deltaIdealV == 0)
                {
                    _race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].idealV = _race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].idealV / 2;
                }
                else
                {
                    _race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].metMaxV = true;
                    for (int i = 0; i < _race.Track.NoOfLanes; i++)
                    {
                        if (_race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].idealV > _race.Track.Pieces[_lastIndex].Lanes[i].idealV)
                        {
                            _race.Track.Pieces[_lastIndex].Lanes[i].idealV = (_race.Track.Pieces[_lastIndex].Lanes[i].idealV + _race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].idealV) / 2;
                        }
                        Console.WriteLine("CRASHED-SET TP:" + _lastIndex + " L:" + i + "IdealV:" + _race.Track.Pieces[_lastIndex].Lanes[i].idealV);
                    }
                    //the actual reduction
                    _race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].idealV = _race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].idealV - _race.Track.Pieces[_lastIndex].Lanes[_currentRaceData.lane].deltaIdealV;
                }

                //take a bit of speed off the last corner too!
                if (_lastIndex == 0)
                {
                    _race.Track.Pieces[_race.Track.Pieces.Count - 1].Lanes[(_currentRaceData.lane)].idealV = _race.Track.Pieces[_race.Track.Pieces.Count - 1].Lanes[(_currentRaceData.lane)].idealV - _race.Track.Pieces[_race.Track.Pieces.Count - 1].Lanes[_currentRaceData.lane].deltaIdealV;

                    _race.Track.Pieces[_race.Track.Pieces.Count - 2].Lanes[(_currentRaceData.lane)].idealV = _race.Track.Pieces[_race.Track.Pieces.Count - 2].Lanes[(_currentRaceData.lane)].idealV - _race.Track.Pieces[_race.Track.Pieces.Count - 2].Lanes[_currentRaceData.lane].deltaIdealV;
                }
                else
                {
                    for (int i = 1; i < 5; i++)
                    {
                        DecreaseSpeedOfPieces(i);
                    }
                }
            }
        }

        public void DecreaseSpeedOfPieces(int j)
        {
            if (_race.Track.Pieces.Count > RelativePieceID(_lastIndex, -j))
            {

            }

            _race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[(_currentRaceData.lane)].idealV = _race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[(_currentRaceData.lane)].idealV - _race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[_currentRaceData.lane].deltaIdealV - 0.5;

            _race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[_currentRaceData.lane].metMaxV = true;

            for (int i = 0; i < _race.Track.NoOfLanes; i++)
            {
                if (_race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[_currentRaceData.lane].idealV > _race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[i].idealV)
                {
                    _race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[i].idealV = (_race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[i].idealV + _race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[_currentRaceData.lane].idealV) / 2;
                }
                Console.WriteLine("CRASHED-SET TP:" + RelativePieceID(_lastIndex, -j) + " L:" + i + " IdealV:" + _race.Track.Pieces[RelativePieceID(_lastIndex, -j)].Lanes[i].idealV);
            }
        }
        #endregion

        #region Post-Race Processing
#if DEBUG
        private void ExportDate(Join join)
        {
            Console.WriteLine("Exporting Data");

            CSVUtil csvUtil = new CSVUtil();

            string csv = csvUtil.GetCsv(_raceData);

            System.IO.StreamWriter streamWriter;

            streamWriter = new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\rd-" + join.trackName + "-" + join.name + " - " + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".csv", true);

            streamWriter.WriteLine(csv);

            streamWriter.Close();
        }
#endif

        private void printTrack()
        {
            Console.WriteLine("Printing Track :::");

            for (int i = 0; i < _race.Track.NoOfPieces; i++)
            {
                for (int j = 0; j < _race.Track.NoOfLanes; j++)
                {
                    Console.WriteLine("TP: " + i + " MAX-VEL-SET: " + _race.Track.Pieces[i].Lanes[j].metMaxV + " IdealV " + _race.Track.Pieces[i].Lanes[j].idealV + " IsCorner: " + _race.Track.Pieces[i].PieceType.ToString());
                }
            }
        }

        #endregion
    }

    public class Corner
    {
        public double radius { private set; get; }
        public float angle { private set; get; }
        public List<int> pieceIds { private set; get; }

        public Corner(double radius, float angle, List<int> pieceIds)
        {
            this.radius = radius;
            this.angle = angle;
            this.pieceIds = pieceIds;
        }
    }

    #region Messages
    class MsgWrapper
    {
        public string msgType;
        public Object data;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }

    abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()), Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }

        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }

    class Join : SendMsg
    {
        public int? carCount;
        public string trackName;
        public string key;
        public string name;
        public string password;
        public float throttle;

        /// <summary>
        /// Default Join
        /// </summary>
        /// <param name="name"></param>
        /// <param name="key"></param>
        public Join(BotId botid)
        {
            this.key = botid.key;
            this.name = botid.name;
        }

        public Join(BotId botid, int carcount)
        {
            this.key = botid.key;
            this.name = botid.name;
            this.carCount = carcount;
        }

        /// <summary>
        /// Join a race with that car count, and track name.
        /// </summary>
        /// <param name="botid"></param>
        /// <param name="carcount"></param>
        /// <param name="trackname"></param>
        public Join(BotId botid, int carcount, string trackname)
        {
            this.key = botid.key;
            this.name = botid.name;
            this.carCount = carcount;
            this.trackName = trackname;
        }

        /// <summary>
        /// Join a race with that car count, and track name.
        /// </summary>
        /// <param name="botid"></param>
        /// <param name="carcount"></param>
        /// <param name="trackname"></param>
        public Join(BotId botid, string password, int carcount, string trackname)
        {
            this.key = botid.key;
            this.name = botid.name;
            this.carCount = carcount;
            this.trackName = trackname;
            this.password = password;
        }

        /// <summary>
        /// Join a race with that car count, and track name.
        /// </summary>
        /// <param name="botid"></param>
        /// <param name="carcount"></param>
        /// <param name="trackname"></param>
        public Join(BotId botid, string password, int carcount, string trackname, float throttle)
        {
            this.key = botid.key;
            this.name = botid.name;
            this.carCount = carcount;
            this.trackName = trackname;
            this.password = password;
            this.throttle = throttle;
        }

        protected override string MsgType()
        {
            return "join";
        }
    }

    class Create : SendMsg
    {
        public int? carCount;
        public string trackName;
        public string password;
        public BotId botId;

        public Create()
        {

        }

        public Create(BotId botid, int carcount, string trackname)
        {
            this.botId = botid;
            this.carCount = carcount;
            this.trackName = trackname;
        }

        public Create(BotId botid, string password, int carcount, string trackname)
        {
            this.botId = botid;
            this.carCount = carcount;
            this.trackName = trackname;
            this.password = password;
        }


        protected override string MsgType()
        {
            return "createRace";
        }
    }

    class JoinRace : SendMsg
    {
        public int? carCount;
        public string trackName;
        public string password;
        public BotId botId;

        public JoinRace()
        {

        }

        /// <summary>
        /// Default Join
        /// </summary>
        /// <param name="name"></param>
        /// <param name="key"></param>
        public JoinRace(BotId botid, string trackName, int carCount)
        {
            this.botId = botid;
            this.trackName = trackName;
            this.carCount = carCount;
        }

        /// <summary>
        /// Default Join
        /// </summary>
        /// <param name="name"></param>
        /// <param name="key"></param>
        public JoinRace(BotId botid, string trackName, int carCount, string password)
        {
            this.botId = botid;
            this.trackName = trackName;
            this.carCount = carCount;
            this.password = password;
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }

    class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }

    class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }

    class SwitchLane : SendMsg
    {
        public string value;

        public SwitchLane(int laneIndex)
        {
            if (laneIndex == 0)
            {
                value = "Left";
            }
            else
            {
                value = "Right";
            }
        }

        protected override object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }

    class Turbo : SendMsg
    {
        public string value;

        public Turbo()
        {
            this.value = "Risking it for a biscuit";
        }

        protected override object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "turbo";
        }
    }

    #endregion

    #region Track Data
    class Race
    {
        public Track Track { private set; get; }
        public RaceSession RaceSession { private set; get; }
        public Cars Cars { private set; get; }

        public Race(Dictionary<string, object> data)
        {
            string raceJson = data["track"].ToString();
            Dictionary<string, object> trackDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(raceJson);
            Track = new Track(trackDict);

            raceJson = data["raceSession"].ToString();
            Dictionary<string, object> raceSessionDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(raceJson);
            RaceSession = new RaceSession(raceSessionDict);

            Cars = new Cars(data);
        }
    }

    class Track
    {
        public string ID { private set; get; }
        public string Name { private set; get; }
        public List<TrackPiece> Pieces { private set; get; }
        public int NoOfPieces { private set; get; }
        public List<Lane> Lanes { private set; get; }
        public int NoOfLanes { private set; get; }
        public int NoOfSwitches { private set; get; }
        public Dictionary<string, List<Corner>> CornersDic { private set; get; }

        public Track(Dictionary<string, object> data)
        {
            Pieces = new List<TrackPiece>();
            ID = data["id"] as string;
            Name = data["name"] as string;

            List<Dictionary<string, object>> pieceData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(data["pieces"].ToString());
            NoOfPieces = pieceData.Count;

            Lanes = new List<Lane>();
            List<Dictionary<string, object>> laneData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(data["lanes"].ToString());
            NoOfLanes = laneData.Count;

            for (int i = 0; i < NoOfLanes; ++i)
            {
                Lane lane = new Lane(laneData[i]);
                Lanes.Add(lane);
            }

            for (int i = 0; i < NoOfPieces; ++i)
            {
                TrackPiece piece = new TrackPiece(pieceData[i]);
                Pieces.Add(piece);

                if (piece.IsSwitch)
                {
                    NoOfSwitches = NoOfSwitches + 1;
                }
            }

            CornersDic = new Dictionary<string, List<Corner>>();
        }

        public PieceType GetPieceType(int index)
        {
            return Pieces[index].PieceType;
        }
    }

    enum PieceType
    {
        RightCorner,
        LeftCorner,
        Straight
    }

    class TrackPiece
    {
        public double Length { private set; get; }
        public PieceType PieceType { private set; get; }
        public bool IsSwitch { private set; get; }
        public double Radius { private set; get; }
        public float Angle { private set; get; }
        public List<Lane> Lanes { private set; get; }
        public string cornerKey { set; get; }

        public TrackPiece(Dictionary<string, object> data)
        {
            IsSwitch = false;
            PieceType = PieceType.Straight;
            Lanes = new List<Lane>();
            cornerKey = "";

            if (data.ContainsKey("length"))
            {
                Length = Convert.ToSingle(data["length"]);
            }
            if (data.ContainsKey("switch"))
            {
                if (Convert.ToBoolean(data["switch"]))
                {
                    IsSwitch = true;
                }
            }
            if (data.ContainsKey("radius"))
            {
                Radius = Convert.ToSingle(data["radius"]);
            }
            if (data.ContainsKey("angle"))
            {
                Angle = Convert.ToSingle(data["angle"]);
                if (Angle >= 0)
                {
                    PieceType = PieceType.RightCorner;
                }
                else
                {
                    PieceType = PieceType.LeftCorner;
                }
            }
        }

    }

    class Lane
    {
        public double DistanceFromCenter { private set; get; }
        public int Index { private set; get; }
        public double Length { private set; get; }
        public double idealV { set; get; }
        public double deltaIdealV { set; get; }
        public bool metMaxV { set; get; }

        public Lane(double Length, double idealV, double deltaIdealV)
        {
            this.Length = Length;
            this.idealV = idealV;
            this.deltaIdealV = deltaIdealV;
            this.metMaxV = false;
        }

        public Lane(Dictionary<string, object> data)
        {
            DistanceFromCenter = Convert.ToDouble(data["distanceFromCenter"]);
            Index = Convert.ToInt32(data["index"]);
        }
    }

    class TurboData
    {
        public int TurboDurationMilliseconds { private set; get; }
        public int TurboDurationTicks { private set; get; }
        public int TurboFactor { private set; get; }

        public TurboData(Dictionary<string, object> data)
        {
            TurboDurationMilliseconds = Convert.ToInt32(data["turboDurationMilliseconds"]);
            TurboDurationTicks = Convert.ToInt32(data["turboDurationTicks"]);
            TurboFactor = Convert.ToInt32(data["turboFactor"]);
        }

    }

    class RaceSession
    {
        public int? laps { private set; get; }
        public int? maxLapTimeMs { private set; get; }
        public bool quickRace { private set; get; }
        public int? durationMs { private set; get; }

        public RaceSession(Dictionary<string, object> data)
        {
            if (data.Count == 1)
            {
                durationMs = Convert.ToInt32(data["durationMs"]);
            }
            else
            {
                laps = Convert.ToInt32(data["laps"]);
                maxLapTimeMs = Convert.ToInt32(data["maxLapTimeMs"]);
                quickRace = Convert.ToBoolean(data["quickRace"]);
            }
        }
    }

    class Cars
    {
        public List<Car> cars { private set; get; }

        public Cars(Dictionary<string, object> data)
        {
            List<Dictionary<string, object>> carsData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(data["cars"].ToString());

            cars = new List<Car>();

            int NoOfCars = carsData.Count;
            for (int i = 0; i < NoOfCars; ++i)
            {
                Car car = new Car(carsData[i]);
                cars.Add(car);
            }
        }
    }

    class Car
    {
        public string name { private set; get; }
        public double length { private set; get; }

        public double inPieceDistance { set; get; }
        public int endLane { set; get; }
        public int startLane { set; get; }
        public int pieceIndex { set; get; }

        public Car(Dictionary<string, object> data)
        {
            string raceJson = data["id"].ToString();
            Dictionary<string, object> idDic = JsonConvert.DeserializeObject<Dictionary<string, object>>(raceJson);
            name = idDic["name"].ToString();

            raceJson = data["dimensions"].ToString();
            Dictionary<string, object> dimenDic = JsonConvert.DeserializeObject<Dictionary<string, object>>(raceJson);
            length = Convert.ToDouble(dimenDic["length"]);
        }
    }
    #endregion

    #region Race Data

    class BotId
    {
        public string name { private set; get; }
        public string key { private set; get; }

        public BotId()
        {

        }

        public BotId(string name, string key)
        {
            this.name = name;
            this.key = key;
        }
    }

    class RaceData
    {
        public RaceData()
        {

        }

        public RaceData(int PieceIndex, int InPieceDistance, int Lane, int Lap, float Angle, int LocalTickCount, float TrackAngle, float TrackLength, string TrackType, float TrackRadius, float Throttle, bool TurboActive, double CurrentPosition, double CurrentSpeed, double CurrentAcceleration)
        {
            this.localTickCount = LocalTickCount;
            this.pieceIndex = PieceIndex;
            this.inPieceDistance = InPieceDistance;
            this.lane = Lane;
            this.lap = Lap;
            this.angle = Angle;
            this.trackAngle = TrackAngle;
            this.trackLength = TrackLength;
            this.trackRadius = TrackRadius;
            this.trackType = TrackType;
            this.thorttle = Throttle;
            this.turboActive = TurboActive;
            this.currentSpeed = CurrentPosition;
            this.currentAcceleration = CurrentSpeed;
            this.currentDeltaAcceleration = CurrentAcceleration;
        }

#if DEBUG
        // Local Tick Count
        [DataMember]
        [CsvColumnNameAttribute(Name = "Local Tick Count", Order = 0)]
#endif
        public int localTickCount { get; set; }

#if DEBUG
        // Car Specific
        [DataMember]
        [CsvColumnNameAttribute(Name = "Piece Index", Order = 2)]
#endif
        public int pieceIndex { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "In Piece Distance", Order = 3)]
#endif
        public double inPieceDistance { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Lane", Order = 4)]
#endif
        public int lane { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Lap", Order = 5)]
#endif
        public int lap { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Angle", Order = 6)]
#endif
        public double angle { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Throttle", Order = 7)]
#endif
        public double thorttle { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Turbo Active", Order = 8)]
#endif
        public bool turboActive { get; set; }

#if DEBUG
        // Track Piece Specfic
        [DataMember]
        [CsvColumnNameAttribute(Name = "Track Length", Order = 9)]
#endif
        public double trackLength { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Track Angle", Order = 10)]
#endif
        public float trackAngle { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Track Radius", Order = 11)]
#endif
        public double trackRadius { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Track Type", Order = 12)]
#endif
        public string trackType { get; set; }

#if DEBUG
        //Speed Specific
        [DataMember]
        [CsvColumnNameAttribute(Name = "Current Speed", Order = 13)]
#endif
        public double currentSpeed { get; set; }
#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Current Acceleration", Order = 14)]
#endif
        public double currentAcceleration { get; set; }
#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "Current Delta Acceleration", Order = 15)]
#endif
        public double currentDeltaAcceleration { get; set; }

#if DEBUG
        [DataMember]
        [CsvColumnNameAttribute(Name = "IsSwitch", Order = 16)]
#endif
        public bool isSwitch { get; set; }

        [DataMember]
        [CsvColumnNameAttribute(Name = "Crashed", Order = 17)]
        public bool crashed { get; set; }
    }

    #endregion

}
